﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public Slider MasterVolumeSlider;
    public Slider MusicVolumeSlider;
    public Slider SfxVolumeSlider;
    public Dropdown QualitySettings;
    public Dropdown ResolutionSettings;
    public Toggle Fullscreen;

    public void Start()
    {

    }

    public void MasVolChange()
    {

    }

    public void MusVolChange()
    {

    }

    public void SfxVolChange()
    {

    }

    public void QualityChange()
    {

    }

    public void ResolutionChange()
    {

    }

    public void FullscreenChange()
    {

    }

    public void SaveOptions()
    {

    }

    public void LoadOptions()
    {

    }
}
