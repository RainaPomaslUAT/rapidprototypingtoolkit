﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManagement : MonoBehaviour
{
    public AudioMixer masterAudio;
    public AnimationCurve volumeCurve;
    public List<string> MixerGroupNameList;
    
    public void VolumeChange(string mixerChannel, float value)
    {
        masterAudio.SetFloat(mixerChannel, volumeCurve.Evaluate(value));
        PlayerPrefs.SetFloat(mixerChannel, value);
    }

    public void LoadVolumeSettings()
    {
        foreach (string name in MixerGroupNameList)
        {
            // TODO: Load the volume settings from PlayerPrefs.
        }
    }
}
