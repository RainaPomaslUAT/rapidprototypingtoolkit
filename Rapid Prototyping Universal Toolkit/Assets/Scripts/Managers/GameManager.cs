﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static SoundManagement soundMan;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
        if (soundMan == null)
        {
            SoundManagement test = GetComponent<SoundManagement>();
            if (test == null)
            {
                test = gameObject.AddComponent<SoundManagement>();
            }

            soundMan = test;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
